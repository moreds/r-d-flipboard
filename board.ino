/*************************************************** 
  This is an example for our Adafruit 16-channel PWM & Servo driver
  Servo test - this will drive 16 servos, one after the other

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/815

  These displays use I2C to communicate, 2 pins are required to  
  interface. For Arduino UNOs, thats SCL -> Analog 5, SDA -> Analog 4

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// you can also call it with a different address you want
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!
#define SERVOMIN  150 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  600 // this is the 'maximum' pulse length count (out of 4096)

int TOTAL_SERVOS = 15;

// our servo # counter
uint8_t servonum = 0;

int currentDegree = 0;


int test_icon[] = {
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0
};

int icon_one_full[] = {
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0
};

int icon_two_full[] = {
  90, 90, 90, 90, 90,
  90, 90, 90, 90, 90,
  90, 90, 90, 90, 90
};

int icon_three_full[] = {
  180, 180, 180, 180, 180,
  180, 180, 180, 180, 180,
  180, 180, 180, 180, 180
};

int icon_arrow_up[] = {
  0,  0,  90, 0, 0,
  0,  90, 90, 90, 0,
  90, 0,  90, 0, 90
 };

 int icon_arrow_down[] = {
  90,  0,  90, 0,  90,
  0,   90, 90, 90, 0,
  0,   0,  90, 0,  0
 };

 int icon_cross[] = {
  0,  0,  90, 0,  0,
  90, 90, 90, 90, 90,
  0,  0,  90, 0,  0
 };

int counter = 0;

int _selectedView[15];

const int buzzer = 9; //buzzer to arduino pin 9
const int knockSensor = A0;

int motionInputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int pirValue = 0;                    // variable for reading the pin status


void setup() {
  pinMode(motionInputPin, INPUT);     // declare sensor as input
  
  Serial.begin(9600);
  Serial.println("16 channel Servo test!");

  //overrideArray(test_icon);
  overrideArray(icon_one_full);
  
  pwm.begin();
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates
  
  yield();
}

// you can use this function if you'd like to set the pulse length in seconds
// e.g. setServoPulse(0, 0.001) is a ~1 millisecond pulse width. its not precise!
void setServoPulse(uint8_t n, double pulse) {
  double pulselength;
  pulselength = 1000000;   // 1,000,000 us per second
  pulselength /= 60;   // 60 Hz
  Serial.print(pulselength); Serial.println(" us per period"); 
  pulselength /= 4096;  // 12 bits of resolution
  Serial.print(pulselength); Serial.println(" us per bit"); 
  pulse *= 1000;
  pulse /= pulselength;
  Serial.println(pulse);
  pwm.setPWM(n, 0, pulse);
}

void commitServoUpdate() {
  for(int i = -1; ++i < TOTAL_SERVOS; ){
    double pulselength_SH = map(_selectedView[i], 0, 180, SERVOMIN, SERVOMAX);
    pwm.setPWM(i, 0, pulselength_SH);
    delay(100);  
  }
  
  delay(1000);
  
}

void updateShow() {

  commitServoUpdate();
  
  servonum++;
  if (servonum == 1) {
    overrideArray(icon_two_full);
  }
  else if (servonum == 2) {
    overrideArray(icon_three_full);
  }
  else if (servonum == 3) {
    overrideArray(icon_one_full);
  }
  else if (servonum == 4) {
    overrideArray(icon_arrow_up);
  }
  else if (servonum == 5) {
    overrideArray(icon_one_full);
  }
  else if (servonum == 6) {
    overrideArray(icon_arrow_down);
  }
  else if (servonum == 7) {
    overrideArray(icon_one_full);
  }
  else if (servonum == 8) {
    overrideArray(icon_cross);
  }
  else {
    servonum = 0;
    overrideArray(icon_one_full);
    
  }
}

void loop() {
  // Drive each servo one at a time
  
  if(updateMotionSensor() == HIGH) {
    updateShow();
  }
  else if(updateKnockSensor() == HIGH) {
    updateShow();
  }
  //overrideArray(test_icon);
  //commitServoUpdate();
  
  
}

void overrideArray(int selectedArray[]) {
  for(int i = -1; ++i < TOTAL_SERVOS; ){
    _selectedView[i] = selectedArray[i];
  }
}




/*
 * PIR sensor tester
 */
int updateMotionSensor(){
  pirValue = digitalRead(motionInputPin);  // read input value
  Serial.println(pirValue);
  if (pirValue == HIGH) {            // check if the input is HIGH
    if (pirState == LOW) {
      // we have just turned on
      Serial.println("Motion detected!");
      // We only want to print on the output change, not state
      pirState = HIGH;
    }
  } else {
    if (pirState == HIGH){
      // we have just turned of
      Serial.println("Motion ended!");
      // We only want to print on the output change, not state
      pirState = LOW;
    }
  }

  return pirState;
}

int updateKnockSensor() {
  byte val = analogRead(knockSensor);   
  int THRESHOLD = 50;
  if(val >= THRESHOLD) return HIGH;
  return LOW;
}


