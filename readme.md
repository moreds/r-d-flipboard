# R&D Flipboard

A prototype using Arduino hardware and software for creating an interactive flipboard.

![picture](assets/img-1.JPG)

![picture](assets/img-2.JPG)

![picture](assets/img-3.JPG)

## Getting Started

To ge started you need: 

- Arduino board.
- Adafruit servo shield
- 15 servo, 180 degrees.
- Knock sensor
- PIR Motion sensor

### Prerequisites

To use Adafruit servo shield install the library located inside 

```
libraries/Adafruit_PWMServoDriver
```

to your local library. *Note*, you might need to synchronise the shield if your using a new one.

### Installing

You need Arduino software to compile source code to your arduino board.

## Deployment

Use arduino software to compile c/c++ code.

## Built With

* [Arduino](https://www.arduino.cc/) - Arduino
* [Ada fruit servo sheild](https://www.adafruit.com/product/1438) - Servo sheild

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Using convention of master and develop branch. For updates and work in progress use the develop branch. For production ready state use master.

## Authors

* [per.borin@shiftstudio.se](per.borin@shiftstudio.se)
* [mansoor.munib@shiftstudio.se](mansoor.munib@shiftstudio.se) 
* [gabriella.karlsson@shiftstudio.se](gabriella.karlsson@shiftstudio.se) 
* [marlene.karlsson@shiftstudio.se](marlene.hernbrand@shiftstudio.se) 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
